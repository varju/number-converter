= The assignment =

Create a utility class that can take in a primitive double and will return a String containing the English representation of that number.

For example, an input of 102564.021 will return a string containing "one hundred and two thousand, five hundred and sixty-four and twenty-one one thousandths"

== Other requirements ==

The utility needs to be able to handle any numbers from 0 to a minimum of 1,000,000,000 (inclusive)

The number can also contain a fractional part to 3 decimal places.  They should be described in tenths, hundreds, or thousandths as appropriate.

== Non-functional requirements ==

- The code should be appropriately unit tested.
- The code should be appropriately commented and javadoc'ed
- The code should be written as it was intended for production.


= Varju Alterations =

The primary difference with what I built versus what was requested is that I am not using the "and" and "," separators in the result.  There are two reasons for this:
1. It is harder to implement.
2. It leads to ambiguous answers.  e.g. Both 100.011 and 0.111 would give the result "one hundred and eleven one thousandths"
