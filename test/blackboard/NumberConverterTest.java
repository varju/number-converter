package blackboard;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class NumberConverterTest {
  private NumberConverter converter;

  @Before
  public void setup() {
    converter = new NumberConverter();
  }

  @Test
  public void integersToNineteen() {
    assertEquals("zero", converter.convert(0));
    assertEquals("one", converter.convert(1));
    assertEquals("sixteen", converter.convert(16));
    assertEquals("seventeen", converter.convert(17));
    assertEquals("eighteen", converter.convert(18));
    assertEquals("nineteen", converter.convert(19));
  }

  @Test
  public void integersLessThanOneHundred() {
    assertEquals("twenty", converter.convert(20));
    assertEquals("thirty-one", converter.convert(31));
    assertEquals("seventy", converter.convert(70));
    assertEquals("ninety-nine", converter.convert(99));
  }

  @Test
  public void integersLessThanOneThousand() {
    assertEquals("one hundred", converter.convert(100));
    assertEquals("two hundred forty-two", converter.convert(242));
    assertEquals("nine hundred ninety-nine", converter.convert(999));
  }

  @Test
  public void integersLessThanTenThousand() {
    assertEquals("one thousand", converter.convert(1000));
    assertEquals("four thousand five hundred sixty-seven", converter.convert(4567));
    assertEquals("eight thousand fourteen", converter.convert(8014));
    assertEquals("nine thousand nine hundred ninety-nine", converter.convert(9999));
  }

  @Test
  public void integersLessThanOneMillion() {
    assertEquals("ten thousand", converter.convert(10000));
    assertEquals("ten thousand twelve", converter.convert(10012));
    assertEquals("two hundred thousand sixty-seven", converter.convert(200067));
    assertEquals("four hundred fifty-six thousand seven hundred fifteen", converter.convert(456715));
    assertEquals("nine hundred ninety-nine thousand nine hundred ninety-nine", converter.convert(999999));
  }

  @Test
  public void integersLessThanOneBillion() {
    assertEquals("one million", converter.convert(1000000));
    assertEquals("one million thirteen", converter.convert(1000013));
    assertEquals("twenty-three million four hundred", converter.convert(23000400));
    assertEquals("forty-five million sixty-seven thousand eighty-nine", converter.convert(45067089));
    assertEquals("nine hundred ninety-nine million nine hundred ninety-nine thousand nine hundred ninety-nine",
        converter.convert(999999999));
  }

  @Test
  public void numbersUpToOneTrillian() {
    assertEquals("one billion", converter.convert(1000000000));
    assertEquals("one billion two", converter.convert(1000000002));
    assertEquals(
        "nine hundred ninety-nine billion nine hundred ninety-nine million nine hundred ninety-nine thousand nine hundred ninety-nine",
        converter.convert(999999999999.0));
  }

  @Test
  public void floatWithOneDecimal() {
    assertEquals("one tenth", converter.convert(0.1));
    assertEquals("two tenths", converter.convert(0.2));
    assertEquals("one hundred and nine tenths", converter.convert(100.9));
    assertEquals("one hundred", converter.convert(100.0));

    // extra decimal places should be ignored
    assertEquals("one tenth", converter.convert(0.100));
  }

  @Test
  public void floatWithTwoDecimals() {
    assertEquals("one hundredth", converter.convert(0.01));
    assertEquals("eleven hundredths", converter.convert(0.11));
    assertEquals("seven and ninety-nine hundredths", converter.convert(7.99));

    // extra decimal places should be ignored
    assertEquals("one hundredth", converter.convert(0.010));
  }

  @Test
  public void floatWithThreeDecimals() {
    assertEquals("one thousandth", converter.convert(0.001));
    assertEquals("one hundred eleven thousandths", converter.convert(0.111));
    assertEquals("seven and nine hundred ninety-nine thousandths", converter.convert(7.999));
    assertEquals(
        "nine hundred ninety-nine million nine hundred ninety-nine thousand nine hundred ninety-nine and nine hundred ninety-nine thousandths",
        converter.convert(999999999.999));
    assertEquals("nine hundred ninety-nine billion and one thousandth", converter.convert(999000000000.001));

    // extra decimal places should be ignored
    assertEquals("one thousandth", converter.convert(0.0010));
  }

  @Test
  public void floatWithFourDecimals() {
    assertEquals("one tenth", converter.convert(0.1001));
    assertEquals("eleven hundredths", converter.convert(0.1101));
    assertEquals("one hundred eleven thousandths", converter.convert(0.1111));
    assertEquals("seven and nine hundred ninety-nine thousandths", converter.convert(7.9991));
  }

  @Test
  public void floatSmallerThanOneThousandth() {
    assertEquals("zero", converter.convert(0.0009));
  }

  @Test(expected = IllegalArgumentException.class)
  public void integerLowerBounds() {
    converter.convert(-1);
  }

  @Test(expected = IllegalArgumentException.class)
  public void integerUpperBounds() {
    converter.convert(1000000000000L);
  }

  @Test(expected = IllegalArgumentException.class)
  public void floatLowerBounds() {
    converter.convert(-0.001);
  }

  @Test(expected = IllegalArgumentException.class)
  public void floatUpperBounds() {
    converter.convert(1000000000000.0);
  }

  @Test
  public void convertAmbiguousNumber() {
    assertEquals("one hundred and eleven thousandths", converter.convert(100.011));
    assertEquals("one hundred eleven thousandths", converter.convert(0.111));
  }
}
