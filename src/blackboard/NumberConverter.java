package blackboard;

import java.math.BigDecimal;

public class NumberConverter {
  /**
   * Converts an integer in the range 0 to 999,999,999,999 to its English equivalent
   */
  public String convert(long i) {
    if (i < 0)
      throw new IllegalArgumentException("Less than minimum supported value: " + i);
    else if (i < 20)
      return intLessThanTwenty((int) i);
    else if (i < 100)
      return intLessThanOneHundred(i);
    else if (i < 1000)
      return intLessThanOneThousand(i);
    else if (i < 1000000)
      return intLessThanOneMillion(i);
    else if (i < 1000000000)
      return intLessThanOneBillion(i);
    else if (i < 1000000000000L)
      return intLessThanOneTrillion(i);
    else
      throw new IllegalArgumentException("More than maximum supported value: " + i);
  }

  private static final String INT_NAMES[] = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight",
      "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen",
      "nineteen" };

  private String intLessThanTwenty(int i) {
    return INT_NAMES[i];
  }

  private String intLessThanOneHundred(long i) {
    String tens = tensString(i / 10);
    long remainder = i % 10;
    if (remainder == 0)
      return tens;
    else
      return tens + "-" + convert(remainder);
  }

  private String tensString(long i) {
    switch ((int) i) {
    // @formatter:off
      case 2: return "twenty";
      case 3: return "thirty";
      case 4: return "forty";
      case 5: return "fifty";
      case 6: return "sixty";
      case 7: return "seventy";
      case 8: return "eighty";
      case 9: return "ninety";
      default: throw new IllegalArgumentException(i + " is invalid");
      // @formatter:on
    }
  }

  private String intLessThanOneThousand(long i) {
    return intLessThanHelper(i, 100, "hundred");
  }

  private String intLessThanOneMillion(long i) {
    return intLessThanHelper(i, 1000, "thousand");
  }

  private String intLessThanOneBillion(long i) {
    return intLessThanHelper(i, 1000000, "million");
  }

  private String intLessThanOneTrillion(long i) {
    return intLessThanHelper(i, 1000000000, "billion");
  }

  private String intLessThanHelper(long i, long scale, String name) {
    String valueInWords = convert(i / scale) + " " + name;
    long remainder = i % scale;
    if (remainder == 0)
      return valueInWords;
    else
      return valueInWords + " " + convert(remainder);
  }

  /**
   * Converts a double in the range 0 to 999,999,999,999 to its English equivalent, with three decimal places of
   * precision. Input with more than three decimal places will be truncated. Note that because floats are inherently
   * imprecise, answers won't always be what you expect.
   */
  public String convert(double f) {
    if (f < 0)
      throw new IllegalArgumentException("Less than minimum supported value: " + f);

    // convert to a BigDecimal for more accurate math
    BigDecimal big = BigDecimal.valueOf(f);
    long decimal = big.longValue();
    String decimalStr = convert(decimal);

    BigDecimal remainder = big.subtract(BigDecimal.valueOf(decimal));
    long tenthsRemainder = remainder.scaleByPowerOfTen(1).intValue();
    long hundredthsRemainder = remainder.scaleByPowerOfTen(2).intValue();
    long thousandthsRemainder = remainder.scaleByPowerOfTen(3).intValue();

    if (tenthsRemainder == 0 && hundredthsRemainder == 0 && thousandthsRemainder == 0)
      return decimalStr;

    String fractionStr;
    if (thousandthsRemainder - (10 * hundredthsRemainder) != 0)
      fractionStr = fractionAs(thousandthsRemainder, "thousandth", "thousandths");
    else if (hundredthsRemainder - (10 * tenthsRemainder) != 0)
      fractionStr = fractionAs(hundredthsRemainder, "hundredth", "hundredths");
    else
      fractionStr = fractionAs(tenthsRemainder, "tenth", "tenths");

    if (decimal == 0)
      return fractionStr;
    else
      return decimalStr + " and " + fractionStr;
  }

  private String fractionAs(long i, String singular, String plural) {
    String asStr = convert(i);
    return asStr + " " + (i == 1 ? singular : plural);
  }
}
